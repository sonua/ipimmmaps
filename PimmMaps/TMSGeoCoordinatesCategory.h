//
//  TMSGeoCoordinatesCategory.h
//  PimmMaps
//
//  Created by Mark Holtze on 1/8/15.
//  Copyright (c) 2015 procuro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <NMAKit/NMAKit.h>

#define EARTH_RADIUS 6371000.0

/** Degrees to Radian **/
#define degreesToRadians( degrees ) ( ( degrees ) / 180.0 * M_PI )

/** Radians to Degrees **/
#define radiansToDegrees( radians ) ( ( radians ) * ( 180.0 / M_PI ) )

@interface NMAGeoCoordinates (TMSGeoCoordinatesCategory)
- (NMAGeoCoordinates*) offsetDistance:(double)distance Heading:(double) heading;
@end
