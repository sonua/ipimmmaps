//
//  MapViewController.m
//  PimmMaps
//
//  Created by Mark Holtze on 12/17/14.
//  Copyright (c) 2014 procuro. All rights reserved.
//

#import "MapViewController.h"
#import <NMAKit/NMAKit.h>
#import "iPimmNav/TMSMap.h"
#import "TMSNavMarkers.h"
#import "TMSSimPlayer.h"
#import "TMSOutboundCommands.h"
#import "TMSGeoCoordinatesCategory.h"

@interface MapViewController ()
@property (weak, nonatomic) IBOutlet NMAMapView *mapView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *playerToStart;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *playerReverse;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *playerPlay;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *playerPause;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *playerStep;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *playerFast;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *playerToEnd;

@property (weak, nonatomic) IBOutlet UITextField *usernameText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UITextField *deliveryIdText;

@property (nonatomic) TMSEngine *engine;
@property (nonatomic) TMSNavMarkers* markers;
@property (nonatomic) TMSMap *tmsMap;
@property NMAMapMarker *currentLocation;
@property TMSSimPlayer *simPlayer;
@property TMSNavPlayer *player;
@property SdrReport *targetSdrReport;
@property NSString *mode;
@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _mode = @"navigation";
    _engine = [TMSEngine sharedEngine];
    _markers = [[TMSNavMarkers alloc] initWithMap:_mapView];
    _targetSdrReport = [TMSOutboundCommands route100];
    
    _mapView.copyrightLogoPosition = NMALayoutPositionBottomCenter;
    [self connectToEngine];
    [self createMap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createMap {
    // create map view
    //    this.map = new NavMap(dom.byId("map-canvas"));
    //    this.map.on("ready", lang.hitch(this, this.mapReady));
    //    this.map.render();
    
    // when map view is ready
    [self deliveryUpdate];
    // start trip view
    _tmsMap = [[TMSMap alloc] initWithEngine:_engine MapView:_mapView];
}

- (void)destroyMap {
    if (_tmsMap)
        [_tmsMap destroy];
    _tmsMap = nil;
    
    // destroy complete map view
    //    if (this.map)
    //        this.map.destroy();
    //    this.map = null;
}

- (void)connectToEngine {
    __weak MapViewController* weakSelf = self;
    
    // fixme track subscriptions so they can be cleaned up
    
    [[TMSOutbound global] addObserverForName:@"deliveryupdate" usingBlock:^(NSNotification *note) {
        // on main queue by default
        [weakSelf deliveryUpdate];
        if ([note.userInfo[@"changed"] boolValue]) {
            [weakSelf trackNavigation];
        }
    }];
    [[TMSOutbound global] addObserverForName:@"updatelocation" usingBlock:^(NSNotification *note) {
        // on main queue by default
        [weakSelf updateLocation:note.userInfo[@"location"]];
    }];
    [_engine subscribeForEvent:@"tms/playerChanged" Listener:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf syncPlayerControls];
        });
    }];
    [_engine subscribeForEvent:@"tms/simstatus" Listener:^(id result) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf syncPlayerControls];
        });
    }];
}

- (void)attachSimPlayer {
    _simPlayer = [[TMSSimPlayer alloc] initWithEngine:_engine];
    [_simPlayer setActive:true];
    _player = _simPlayer;
    [self syncPlayerControls];
}

- (void)syncPlayerControls {
    Info* info = _player ? [_player getInfo] : nil;
    BOOL enabled = _player && info && [TMSOutbound global].delivery && [_player isActive];
    
    [_playerToStart setEnabled:(enabled && [_player supports:@"reset"] && !info.atStart)];
    [_playerReverse setEnabled:(enabled && [_player supports:@"reverse"] && !info.atStart)];
    [_playerPlay setEnabled:(enabled && [_player supports:@"play"] && !info.playing && (!info.atEnd || info.replayable))];
    [_playerPause setEnabled:(enabled && [_player supports:@"pause"] && (info.playing || info.reversing))];
    [_playerStep setEnabled:(enabled && [_player supports:@"step"] && !info.atEnd)];
    [_playerFast setEnabled:(enabled && [_player supports:@"fastForward"] && !info.atEnd)];
    [_playerToEnd setEnabled:(enabled && [_player supports:@"playToEnd"] && !info.atEnd)];
}

- (void)deliveryUpdate {
    if (_markers)
        [_markers setForDelivery:[TMSOutbound global].delivery];
    
    [self syncPlayerControls];
}

-(void) trackNavigation {
    _mode = @"navigation";
    
    NMAGeoCoordinates* location = nil;
    if (_currentLocation) {
        location = _currentLocation.coordinates;
    }
    else if ([TMSOutbound global].hasDelivery) {
        NSString* dcId = [TMSOutbound global].delivery.DCId;
        TMSMapMarker* dcMarker = [_markers getSiteById:dcId];
        location = dcMarker.mapMarker.coordinates;
    }
    else {
        location = _mapView.geoCenter;
    }
    
    [self zoomAround: location];
}

-(void) trackRoute {
    _mode = @"route";
    [self zoomAll];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) zoomAround:(NMAGeoCoordinates*) coordinates {
//    double dist = sqrt((500.0*500.0) + (500.0*500.0));
//    NMAGeoCoordinates* bottomRight = [coordinates offsetDistance:dist Heading:135];
//    NMAGeoCoordinates* topLeft = [coordinates offsetDistance:dist Heading:315];
//    NMAGeoBoundingBox* bb = [[NMAGeoBoundingBox alloc] initWithTopLeft:topLeft bottomRight:bottomRight];
//    [_mapView zoomTo:bb animation:NMAMapAnimationNone];
    
    _markers.map.zoomLevel = 16;
    [_mapView setCenterAt:coordinates animation:NMAMapAnimationNone];
}

-(void) gotoCurrentLocation {
    if (_currentLocation) {
        NMAGeoCoordinates* location = _currentLocation.coordinates;
        [_mapView setCenterAt:location animation:NMAMapAnimationNone];
    }
}

-(void) zoomAll {
    NMAGeoBoundingBox* bounds = [_markers getBoundingBox];
    [_mapView zoomTo:bounds animation:NMAMapAnimationNone];
}

-(void) updateLocation:(CLLocation*) newLocation {
    NMAGeoCoordinates *coords = [[NMAGeoCoordinates alloc]
                                 initWithLatitude:newLocation.coordinate.latitude
                                 longitude:newLocation.coordinate.longitude];
    
    if (_currentLocation) {
        _currentLocation.coordinates = coords;
    }
    else {
        _currentLocation = [[NMAMapMarker alloc] initWithGeoCoordinates:coords image:[UIImage imageNamed:@"current_loc"]];
        _currentLocation.zIndex = 10000;
        [_mapView addMapObject:_currentLocation];
    }
    
    if ([_mode isEqual: @"navigation"]) {
        [self gotoCurrentLocation];
    }
}


- (IBAction)onPlayerStart:(id)sender {
    if (_player.isActive && [_player supports:@"reset"])
        [_player reset];
}

- (IBAction)onPlayerReverse:(id)sender {
    if (_player.isActive && [_player supports:@"reverse"])
        [_player reverse];
}

- (IBAction)onPlayerPlay:(id)sender {
    if (_player.isActive && [_player supports:@"play"])
        [_player play];
}

- (IBAction)onPlayerPause:(id)sender {
    if (_player.isActive && [_player supports:@"pause"])
        [_player pause];
}

- (IBAction)onPlayerStep:(id)sender {
    if (_player.isActive && [_player supports:@"step"])
        [_player step];
}

- (IBAction)onPlayerFast:(id)sender {
    if (_player.isActive && [_player supports:@"fastForward"])
        [_player fastForward];
}

- (IBAction)onPlayerEnd:(id)sender {
    if (_player.isActive && [_player supports:@"playToEnd"])
        [_player playToEnd];
}


- (IBAction)onPunchOut:(id)sender {
    if ([TMSOutbound global].hasDelivery)
        [[TMSOutboundCommands global] punchOut];
}

- (IBAction)onPunchInResume:(id)sender {
    if ([TMSOutbound global].hasDelivery)
        [[TMSOutboundCommands global] punchOut];
    
    [[TMSOutboundCommands global] punchIn:[TMSOutbound global].lastSdrReport];
    [[TMSOutboundCommands global] startOrRestoreRoute];
    [[TMSOutboundCommands global] resumeSimulation:[TMSOutboundCommands global].lastSimulation];
}

- (IBAction)onPunchInNew:(id)sender {
    [[TMSOutboundCommands global] startNew:_targetSdrReport];
}

- (IBAction)onEndOfRoute:(id)sender {
}

- (IBAction)onEndNew:(id)sender {
}

- (IBAction)onRestartRoute:(id)sender {
    [[TMSOutboundCommands global] stopCurrentTrip];
    [[TMSOutboundCommands global] punchIn:_targetSdrReport];
    [[TMSOutboundCommands global] startRoute];
    [[TMSOutboundCommands global] resumeSimulation:[TMSOutboundCommands global].lastSimulation];
}


- (IBAction)onMapOff:(id)sender {
    [self destroyMap];
}

- (IBAction)onMapOn:(id)sender {
    if (!_tmsMap)
        [self createMap];
}


- (IBAction)onLoadRoute:(id)sender {
    // TODO: sonia needs to implement this
    
    // 1) init data manager with credentials from the following, timezone can just be device time it does not really matter
    //_usernameText.text
    //_passwordText.text
    
    // 2) using deliveryId from text field try to load the SdrReport
    //_deliveryIdText.text
    
    // 3) on load success
    //_targetSdrReport = loadedSdrdReport;
    //[[TMSOutboundCommands global] startNew:_targetSdrReport];
    
    //on error notify user
}

- (IBAction)onDefaultRoute:(id)sender {
    _targetSdrReport = [TMSOutboundCommands route100];
    [[TMSOutboundCommands global] startNew:_targetSdrReport];
}


@end

