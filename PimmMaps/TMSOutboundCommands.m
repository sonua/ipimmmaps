//
//  TMSOutboundCommands.m
//  PimmMaps
//
//  Created by Mark Holtze on 1/7/15.
//  Copyright (c) 2015 procuro. All rights reserved.
//

#import "TMSOutboundCommands.h"
#import "iPimmNav/iPimmNavLog.h"

@implementation TMSOutboundCommands {
    BOOL _trace;
}

+(TMSOutboundCommands*) global {
    static dispatch_once_t pred = 0;
    __strong static TMSOutboundCommands* _global = nil;
    dispatch_once(&pred, ^{
        _global = [[TMSOutboundCommands alloc] init];
    });
    return _global;
}

+(SdrReport*) route100 {
    static dispatch_once_t pred = 0;
    __strong static SdrReport* _route100 = nil;
    dispatch_once(&pred, ^{
        PimmLogFunctionTrace(@"Enter Function:%s", __func__);
        
        NSError *jsonError = nil;
        NSString* filePath = [[NSBundle mainBundle] pathForResource:@"route_100" ofType:@"json"];
        NSData *jsonData = [[NSData alloc] initWithContentsOfFile:filePath];
        id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
    
        if(jsonError != nil) {
            NSLog(@"Error while reading JSON File for route100");
            return;
        }
        
        SdrReport* sdr = [[SdrReport alloc] init];
        [sdr readFromJSONDictionary:jsonObject];
        _route100 = sdr;
    });
    return _route100;
}

-(id) init {
    if (self = [super init]) {
        _outbound = [TMSOutbound global];
        _engine = [TMSEngine sharedEngine];
        _lastSimulation = nil;
        _trace = false;
    }
    return self;
}

@synthesize lastSimulation = _lastSimulation;

- (NSDictionary*) lastSimulation {
        return _lastSimulation;
}

-(void) enableFeatures:(TMSEngine*) engine {
    if (_trace) NSLog(@"TMSOutboundCommands.enableFeatures");
    [_engine.advanced injectSimulationModule];
    [_engine enableFeatures:@{ @"enableLocalPersistence": [NSNumber numberWithBool:true],
                               @"navigationEnabled": [NSNumber numberWithBool:true] }];
}

-(void) stopCurrentTrip {
    if (_trace) NSLog(@"TMSOutboundCommands.stopCurrentTrip");
    // preserve last simulation state, if current state is null/undefined, then just keep whatever was last
    _lastSimulation = [_engine.simulator currentOptions];
    
    [_outbound setSdrReport:nil];
    [_engine stop];
}

-(void) punchIn:(SdrReport*) sdrReport {
    if (_trace) NSLog(@"TMSOutboundCommands.punchIn");
    [_outbound setSdrReport:sdrReport];
}

-(void) punchOut {
    if (_trace) NSLog(@"TMSOutboundCommands.punchOut");
    [self stopCurrentTrip];
}

-(void) startNew:(SdrReport*) sdrReport {
    if (_trace) NSLog(@"TMSOutboundCommands.startNew");
    [self stopCurrentTrip];
    [self punchIn:sdrReport];
    [self startRoute];
    [self simulateWithPlannedRoute];
}

-(void) startRoute {
    if (_trace) NSLog(@"TMSOutboundCommands.startRoute");
    [self enableFeatures:_engine];
    [_engine startWithDeliveryData:_outbound.delivery TripProfile:nil Callback:^(NSDictionary *promise) {
        NSString* status =  (NSString*)[promise objectForKey:@"status"];
        if([status compare:@"RESOLVED" ] == NSOrderedSame) {
            if (_trace) NSLog(@"TMSOutboundCommands.startRoute succeeded");
            [self.outbound navigationStarted];
        }
        else if([status compare:@"REJECTED" ] == NSOrderedSame) {
            NSLog(@"TMSOutboundCommands.startRoute failed");
        }
    }];
}

-(void) startOrRestoreRoute {
    if (_trace) NSLog(@"TMSOutboundCommands.startOrRestoreRoute");
    [self enableFeatures:_engine];
    [_engine startOrRestoreWithDeliveryData:_outbound.delivery TripProfile:nil Callback:^(NSDictionary *promise) {
        NSString* status =  (NSString*)[promise objectForKey:@"status"];
        if([status compare:@"RESOLVED" ] == NSOrderedSame) {
            if (_trace) NSLog(@"TMSOutboundCommands.startOrRestoreRoute succeeded");
            [self.outbound navigationStarted];
        }
        else if([status compare:@"REJECTED" ] == NSOrderedSame) {
            NSLog(@"TMSOutboundCommands.startOrRestoreRoute failed");
        }
    }];
}

-(void) endRoute {
    if (_trace) NSLog(@"TMSOutboundCommands.endRoute");
    [_outbound setSdrReport:nil];
    [_engine stop];
}
             
-(void) simulateWithPlannedRoute {
    if (_trace) NSLog(@"TMSOutboundCommands.simulateWithPlannedRoute");
    [_engine.simulator startWithPlannedRoute];
    [_engine.advanced captureGPSPush:@"sim" PreSampled:true];
    _lastSimulation = nil;
}

-(void) resumeSimulation:(NSDictionary*) options {
    if (_trace) NSLog(@"TMSOutboundCommands.resumeSimulation");
    [_engine.simulator resumeWithPlannedRouteUsingOptions:options];
    [_engine.advanced captureGPSPush:@"sim" PreSampled:true];
    _lastSimulation = nil;
}

-(void) pushGPS:(CLLocation*) gpsVal {
    [_outbound updateLocation:gpsVal];
    if (_outbound.navigationRunning) {
        [_engine pushGPS:gpsVal];
    }
}

@end
