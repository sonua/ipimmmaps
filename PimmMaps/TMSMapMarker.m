//
//  TMSMapMarker.m
//  PimmMaps
//
//  Created by Mark Holtze on 12/17/14.
//  Copyright (c) 2014 procuro. All rights reserved.
//

#import "TMSMapMarker.h"
#import "TMSNavMarkers.h"

@implementation TMSMapMarker


-(id) initWithMarkers:(TMSNavMarkers*) markers
                 Type:(NSString*) type
                   Id:(NSString*) locationId
             Location:(id) location {
    if(self = [super init])
    {
        _markers = markers;
        _type = type;
        _locationId = locationId;
        _location = location;
        _mapMarker = [self render];
        [_markers.container addMapObject:_mapMarker];
        return self;
    }
    return self;
}

-(void) destroy {
    if (_mapMarker) {
        [_markers.container removeMapObject:_mapMarker];
        _markers = nil;
        _mapMarker = nil;
    }
}

-(void) update {
    [_markers.container removeMapObject:_mapMarker];
    _mapMarker = [self render];
    [_markers.container addMapObject:_mapMarker];
}


-(SdrStop*) stop { return (SdrStop*) _location; }
-(SdrGisSite*) site { return (SdrGisSite*) _location; }
-(SdrPointOfInterest*) poi { return (SdrPointOfInterest*) _location; }

-(NMAMapMarker*) render {
    NMAGeoCoordinates* coords = [self coordinates];
    if ([self.type isEqual: @"site"]) {
        return [[NMAMapMarker alloc] initWithGeoCoordinates:coords
                                                      image:[UIImage imageNamed:@"dc_loc"]];
    }
    else if ([self.type isEqual: @"poi"]) {
        return [[NMAMapMarker alloc] initWithGeoCoordinates:coords
                                                      image:[UIImage imageNamed:@"unscheduled_loc"]];
    }
    else { // stop
        return [[NMAMapMarker alloc] initWithGeoCoordinates:coords
                                                      image:[UIImage imageNamed:@"stop_loc"]];
    }
}

-(NMAGeoCoordinates*) coordinates {
    LatLon* latLon;
    if ([self.type isEqual: @"site"]) {
        latLon = [self site].LatLon;
    }
    else if ([self.type isEqual: @"poi"]) {
        latLon = [self poi].LatLon;
    }
    else { // stop
        latLon = [self stop].LatLon;
    }
    return [[NMAGeoCoordinates alloc] initWithLatitude:[latLon.Lat doubleValue]
                                             longitude:[latLon.Lon doubleValue]];
}

-(void) focus {
    [_markers.map setCenterAt:_mapMarker.coordinates animation:NMAMapAnimationNone];
    _markers.map.zoomLevel = 16;
}

@end
