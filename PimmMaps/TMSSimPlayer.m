//
//  TMSSimPlayer.m
//  PimmMaps
//
//  Created by Sonia Sharma on 07/10/2014.
//  Copyright (c) 2014 procuro. All rights reserved.
//

#import "TMSSimPlayer.h"
#import "iPimmNav/iPimmNavLog.h"

@implementation TMSSimPlayer

-(id) initWithEngine:(TMSEngine *)engine
{
    PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    if(self = [super initWithEngine:engine])
    {
        return self;
    }
    else
        return nil;
}


-(NSMutableDictionary*) initializeSupports
{
    PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
    [dic setObject:[[NSNumber alloc] initWithBool:false] forKey:@"reset"];
    [dic setObject:[[NSNumber alloc] initWithBool:false] forKey:@"reverse"];
    [dic setObject:[[NSNumber alloc] initWithBool:TRUE] forKey:@"play"];
    [dic setObject:[[NSNumber alloc] initWithBool:TRUE]  forKey:@"pause"];
    [dic setObject:[[NSNumber alloc] initWithBool:TRUE] forKey:@"step"];
    [dic setObject:[[NSNumber alloc] initWithBool:TRUE]  forKey:@"fastForward"];
    [dic setObject:[[NSNumber alloc] initWithBool:TRUE] forKey:@"playToEnd"];
    
    return dic;
}

-(void) reset
{
    PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    NSException* exception = [[NSException alloc] initWithName:@"UnsupportedOperation" reason:@"Reset Operation not supported" userInfo:nil];
    
    @throw exception;
}

-(void) reverse
{
    PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    NSException* exception = [[NSException alloc] initWithName:@"UnsupportedOperation" reason:@"Reverse Operation not supported" userInfo:nil];
    
    @throw exception;

}

-(void) play
{
    PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    @synchronized (self)
    {
        [self clearSpeed];
        [self.engine.simulator play];
    }
}

-(void) pause
{
    PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    @synchronized (self)
    {
        [self clearSpeed];
        [self.engine.simulator stop];
    }
}

-(void) step:(int) size
{
    PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    @synchronized (self)
    {
        [self clearSpeed];
        [self.engine.simulator step:size];
    }
    
}

-(void) fastForward
{
    PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    @synchronized (self)
    {
        [self pause];
        int speed = [self nextSpeed:1];
        if(speed > 0)
        {
            [self.engine.simulator playAtSpeed:speed];
        }
    }
    
}

-(void) playToEnd
{
    PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    @synchronized (self)
    {
        [self clearSpeed];
        [self.engine.simulator step:50000];
    }
    
}

- (Info*) getInfo
{
    NSDictionary* infoMap = [self.engine.simulator getInfo];
    Info* info = [[Info alloc] initWithReady:[infoMap[@"ready"] boolValue]
                                     AtStart:[infoMap[@"atStart"] boolValue]
                                       AtEnd:[infoMap[@"atEnd"] boolValue]
                                     Playing:[infoMap[@"playing"] boolValue]
                                   Reversing:[infoMap[@"reversing"] boolValue]
                                  Replayable:[infoMap[@"replayable"] boolValue]];
    return info;
}

@end
