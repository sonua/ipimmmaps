//
//  TMSNavPlayer.m
//  PimmMaps
//
//  Created by Sonia Sharma on 07/10/2014.
//  Copyright (c) 2014 procuro. All rights reserved.
//

#import "TMSNavPlayer.h"
#import "iPimmNav/iPimmNavLog.h"



@implementation Info


-(id) init
{
    if(self = [super init])
    {
        _ready = false;
        _atStart = false;
        _atEnd = false;
        _playing = false;
        _reversing = false;
        _replayable = false;
        return  self;
    }
    else
        return nil;
    
}
-(id) initWithReady:(BOOL) ready
            AtStart:(BOOL) atStart
              AtEnd:(BOOL) atEnd
            Playing:(BOOL) playing
          Reversing:(BOOL) reversing
         Replayable:(BOOL) replayable
{
    if(self = [super init])
    {
        _ready = ready;
        _atStart = atStart;
        _atEnd = atEnd;
        _playing = playing;
        _reversing = reversing;
        _replayable = replayable;
        return  self;
    }
    else
        return nil;
}

@end

@implementation TMSNavPlayer

-(id) initWithEngine:(TMSEngine*) engine
{
    PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    if(self = [super init])
    {
        _speed = 0;
        _maxSpeed = 0;
        _dir = 1;
        _active = false;
        _engine = engine;
        _supports = [self initializeSupports];
        
        return self;
    }
    else
        return nil;
}

-(NSMutableDictionary*) initializeSupports
{
    PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
    [dic setObject:[[NSNumber alloc] initWithBool:false] forKey:@"reset"];
    [dic setObject:[[NSNumber alloc] initWithBool:false] forKey:@"reverse"];
    [dic setObject:[[NSNumber alloc] initWithBool:false] forKey:@"play"];
    [dic setObject:[[NSNumber alloc] initWithBool:false]  forKey:@"pause"];
    [dic setObject:[[NSNumber alloc] initWithBool:false] forKey:@"step"];
    [dic setObject:[[NSNumber alloc] initWithBool:false]  forKey:@"fastForward"];
    [dic setObject:[[NSNumber alloc] initWithBool:false] forKey:@"playToEnd"];
    return dic;
    
    
    
}

-(BOOL) isActive
{
    
    @synchronized (self)
    {
        return _active;
    }
}
-(void) setActive:(BOOL) active
{
    @synchronized (self)
    {
        _active = active;
    }
    
}

-(int) nextSpeed:(int) dir
{
    @synchronized (self)
    {
        if(_dir != dir)
        {
            _speed = 0;
            _dir = dir;
        }
        _speed = (_speed != 0) ? (_speed *2) : 2;
        if(_speed > _maxSpeed)
        {
            _speed = 0;
        }
        return _speed;
    }
}

-(void) clearSpeed
{
    _speed = 0;
}

-(Info*) getInfo
{
    Info* info = [[Info alloc] init];
    return info;
}

-(void) step
{
    [self step:1];
}

-(BOOL) supports:(NSString*) key {
    NSNumber* entry = _supports[key];
    if (entry)
        return [entry boolValue];
    return false;
}

-(void) reset {}
-(void) reverse {}
-(void) play {}
-(void) pause {}
-(void) step:(int) size {}
-(void) fastForward {}
-(void) playToEnd {}

@end
