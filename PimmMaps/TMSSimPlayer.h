//
//  TMSSimPlayer.h
//  PimmMaps
//
//  Created by Sonia Sharma on 07/10/2014.
//  Copyright (c) 2014 procuro. All rights reserved.
//

#import "TMSNavPlayer.h"

@interface TMSSimPlayer : TMSNavPlayer

-(id) initWithEngine:(TMSEngine *)engine;

-(void) reset;

-(void) reverse;

-(void) play;

-(void) pause;

-(void) step:(int) size;

-(void) fastForward;

-(void) playToEnd;


@end
