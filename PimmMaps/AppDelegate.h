//
//  AppDelegate.h
//  PimmMaps
//
//  Created by Sonia Sharma on 25/08/2014.
//  Copyright (c) 2014 procuro. All rights reserved.
//

//#import <UIKit/UIKit.h>
#import "MapViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
