//
//  TMSMapMarker.h
//  PimmMaps
//
//  Created by Mark Holtze on 12/17/14.
//  Copyright (c) 2014 procuro. All rights reserved.
//

#import <NMAKit/NMAKit.h>
#import <Foundation/Foundation.h>
#import "iPimmDataManagerLib/SdrStop.h"
#import "iPimmDataManagerLib/SdrGisSite.h"
#import "iPimmDataManagerLib/SdrPointOfInterest.h"

@class TMSNavMarkers;

@interface TMSMapMarker : NSObject

@property(nonatomic,readonly, weak) TMSNavMarkers* markers;
@property(nonatomic,readonly) NSString* type;
@property(nonatomic,readonly) NSString* locationId;
@property(nonatomic,readonly, weak) id location;
@property(nonatomic,readonly) NMAMapMarker* mapMarker;

-(id) initWithMarkers:(TMSNavMarkers*) markers
                 Type:(NSString*) type
                 Id:(NSString*) locationId
                 Location:(id) location;

-(SdrStop*) stop;
-(SdrGisSite*) site;
-(SdrPointOfInterest*) poi;

-(void) destroy;
-(void) update;
-(void) focus;

@end
