//
//  TMSGeoCoordinatesCategory.m
//  PimmMaps
//
//  Created by Mark Holtze on 1/8/15.
//  Copyright (c) 2015 procuro. All rights reserved.
//

#import "TMSGeoCoordinatesCategory.h"

@implementation NMAGeoCoordinates (TMSGeoCoordinatesCategory)
-(NMAGeoCoordinates*) offsetDistance:(double)distance Heading:(double) heading {
    // from: http://www.movable-type.co.uk/scripts/latlong.html
    double rad = degreesToRadians(heading);
    double angularDist = distance / EARTH_RADIUS; // angular distance in radians
    
    double lat1 = degreesToRadians(self.latitude);
    double lon1 = degreesToRadians(self.longitude);
    
    double lat2 = asin(sin(lat1)*cos(angularDist) +
                         cos(lat1)*sin(angularDist)*cos(rad));
    double lon2 = lon1 + atan2(sin(rad)*sin(angularDist)*cos(lat1),
                                 cos(angularDist)-sin(lat1)*sin(lat2));
    lon2 = fmod((lon2+3*M_PI), (2*M_PI)) - M_PI; // normalise to -180..+180º
    
    return [[NMAGeoCoordinates alloc] initWithLatitude:radiansToDegrees(lat2) longitude:radiansToDegrees(lon2)];
}
@end
