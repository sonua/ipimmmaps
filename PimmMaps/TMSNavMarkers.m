//
//  TMSNavMarkers.m
//  PimmMaps
//
//  Created by Mark Holtze on 12/17/14.
//  Copyright (c) 2014 procuro. All rights reserved.
//

#import "TMSNavMarkers.h"
#import "iPimmNav/TMSMap.h"

@implementation TMSNavMarkers
-(id) initWithMap:(NMAMapView*) map {
    if(self = [super init])
    {
        _markersZIndex = 1000;
        _map = map;
        _stops = [[NSArray alloc] init];
        _sites = [[NSArray alloc] init];
        _pois = [[NSArray alloc] init];
        _container = [[NMAMapContainer alloc] init];
        _container.zIndex = _markersZIndex;
        [_map addMapObject:_container];
    }
    return self;
}

-(void) destroy {
    if (_container) {
        [_map removeMapObject:_container];
        _container = nil;
        _map = nil;
    }
}

-(NMAGeoBoundingBox*) getBoundingBox {
    NSMutableArray* allCoords = [[NSMutableArray alloc] init];
    
    for (TMSMapMarker* m in _stops) [allCoords addObject:m.mapMarker.coordinates];
    for (TMSMapMarker* m in _sites) [allCoords addObject:m.mapMarker.coordinates];
    for (TMSMapMarker* m in _pois) [allCoords addObject:m.mapMarker.coordinates];
    
    return [NMAGeoBoundingBox geoBoundingBoxContainingGeoCoordinates:allCoords];
}

-(TMSMapMarker*) getStopById:(NSString*) sid {
    NSUInteger index = [_stops indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [((TMSMapMarker*) obj).locationId isEqual:sid];
    }];
    return index != NSNotFound ? [_stops objectAtIndex:index] : nil;
}

-(TMSMapMarker*) getSiteById:(NSString*) sid {
    NSUInteger index = [_sites indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [((TMSMapMarker*) obj).locationId isEqual:sid];
    }];
    return index != NSNotFound ? [_stops objectAtIndex:index] : nil;
}

-(TMSMapMarker*) getPOIById:(NSString*) sid {
    NSUInteger index = [_pois indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [((TMSMapMarker*) obj).locationId isEqual:sid];
    }];
    return index != NSNotFound ? [_stops objectAtIndex:index] : nil;
}

-(void) setForStops:(NSArray*) stops Sites:(NSArray*) sites POIs:(NSArray*) pois {
    [self removeMarkers];
    
    NSMutableArray* stopMarkers = [[NSMutableArray alloc] init];
    for (SdrStop* s in stops) [stopMarkers addObject:[self createStopMarker:s]];
    _stops = stopMarkers;
    
    NSMutableArray* siteMarkers = [[NSMutableArray alloc] init];
    for (SdrGisSite* s in sites) [siteMarkers addObject:[self createSiteMarker:s]];
    _sites = siteMarkers;
    
    NSMutableArray* poiMarkers = [[NSMutableArray alloc] init];
    for (SdrPointOfInterest* p in pois) [poiMarkers addObject:[self createPOIMarker:p]];
    _pois = poiMarkers;
}

-(void) setForDelivery:(SdrDelivery*) delivery {
    if (!delivery)
        [self removeMarkers];
    else
        [self setForStops:delivery.GIS.Stops Sites:delivery.GIS.Sites POIs:delivery.GIS.PointsOfInterest];
}

-(void) removeMarkers {
    for (TMSMapMarker* m in _stops) [m destroy];
    for (TMSMapMarker* m in _sites) [m destroy];
    for (TMSMapMarker* m in _pois) [m destroy];
    _stops = [[NSArray alloc] init];
    _sites = [[NSArray alloc] init];
    _pois = [[NSArray alloc] init];
}

-(TMSMapMarker*) createStopMarker: (SdrStop*) stop {
    TMSMapMarker* m = [[TMSMapMarker alloc] initWithMarkers:self Type:@"stop" Id:stop.SdrStopId Location:stop];
    if (_stopRendered) _stopRendered(stop, m);
    return m;
}

-(TMSMapMarker*) createSiteMarker: (SdrGisSite*) site {
    TMSMapMarker* m = [[TMSMapMarker alloc] initWithMarkers:self Type:@"site" Id:site.SiteId Location:site];
    if (_siteRendered) _siteRendered(site, m);
    return m;
}

-(TMSMapMarker*) createPOIMarker: (SdrPointOfInterest*) poi {
    TMSMapMarker* m = [[TMSMapMarker alloc] initWithMarkers:self Type:@"poi" Id:poi.SdrPointOfInterestId Location:poi];
    if (_poiRendered) _poiRendered(poi, m);
    return m;
}

@end


