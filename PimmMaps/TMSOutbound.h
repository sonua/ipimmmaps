//
//  TMSOutbound.h
//  PimmMaps
//
//  Created by Mark Holtze on 1/7/15.
//  Copyright (c) 2015 procuro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "iPimmDataManagerLib/SdrReport.h"

@interface TMSOutbound : NSObject

+(TMSOutbound*) global;

// Preserve a reference to the last sdrReport. This is for testing to simplify
// stopping/starting trips.
@property(readonly) SdrReport* lastSdrReport;

@property(readonly) SdrReport* sdrReport;
@property(readonly) SdrDelivery* delivery;
@property(readonly) CLLocation* currentLocation;
@property(readonly) BOOL hasDelivery;
@property(readonly) BOOL navigationRunning;

-(id) init;
-(void) setSdrReport:(SdrReport*) sdrReport;
-(void) updateSdrReport:(SdrReport*) sdrReport;
-(void) navigationStarted;
-(void) updateLocation:(CLLocation*) location;

// events: deliverychange, deliveryupdate, updatelocation

- (id<NSObject>) addObserverForName:(NSString *)name
                         usingBlock:(void (^)(NSNotification *note))block;

- (id<NSObject>) addObserverForName:(NSString *)name
                              queue:(NSOperationQueue *)queue
                         usingBlock:(void (^)(NSNotification *note))block;

@end
