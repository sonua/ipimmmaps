//
//  TMSOutboundCommands.h
//  PimmMaps
//
//  Created by Mark Holtze on 1/7/15.
//  Copyright (c) 2015 procuro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMSOutbound.h"
#import "iPimmNav/TMSEngine.h"

@interface TMSOutboundCommands : NSObject

+(TMSOutboundCommands*) global;
+(SdrReport*) route100;

@property(readonly,nonatomic) TMSOutbound* outbound;
@property(readonly,nonatomic) TMSEngine* engine;
@property(readonly) NSDictionary* lastSimulation;

-(id) init;
-(void) stopCurrentTrip;
-(void) punchIn:(SdrReport*) sdrReport;
-(void) punchOut;
-(void) startNew:(SdrReport*) sdrReport;
-(void) startRoute;
-(void) startOrRestoreRoute;
-(void) endRoute;
-(void) simulateWithPlannedRoute;
-(void) resumeSimulation:(NSDictionary*) options;
-(void) pushGPS:(CLLocation*) gpsVal;

@end
