//
//  MapViewController.h
//  PimmMaps
//
//  Created by Mark Holtze on 12/17/14.
//  Copyright (c) 2014 procuro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapViewController : UIViewController

- (void)attachSimPlayer;

@end
