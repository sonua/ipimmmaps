//
//  TMSNavPlayer.h
//  PimmMaps
//
//  Created by Sonia Sharma on 07/10/2014.
//  Copyright (c) 2014 procuro. All rights reserved.
//

#import "iPimmNav/TMSEngine.h"

@interface Info : NSObject
{
}
-(id) init;

-(id) initWithReady:(BOOL) ready
            AtStart:(BOOL) atStart
              AtEnd:(BOOL) atEnd
            Playing:(BOOL) playing
          Reversing:(BOOL) reversing
         Replayable:(BOOL) replayable;

@property(nonatomic, readonly) BOOL ready;
@property(nonatomic, readonly) BOOL atStart;
@property(nonatomic, readonly) BOOL atEnd;
@property(nonatomic, readonly) BOOL playing;
@property(nonatomic, readonly) BOOL reversing;
@property(nonatomic, readonly) BOOL replayable;

@end

@interface TMSNavPlayer : NSObject
{
    int _speed;
    int _maxSpeed;
    int _dir;
    BOOL _active;
    NSMutableDictionary* _supports;
}

@property(nonatomic, readonly) TMSEngine* engine;

-(id) initWithEngine:(TMSEngine*) engine;

-(BOOL) isActive;
-(void) setActive:(BOOL) active;

-(int) nextSpeed:(int) dir;

-(void) clearSpeed;

-(Info*) getInfo;

-(void) step;

-(BOOL) supports:(NSString*) key;

//Abstract Methods

-(void) reset;

-(void) reverse;

-(void) play;

-(void) pause;

-(void) step:(int) size;

-(void) fastForward;

-(void) playToEnd;


@end
