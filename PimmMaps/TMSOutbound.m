//
//  TMSOutbound.m
//  PimmMaps
//
//  Created by Mark Holtze on 1/7/15.
//  Copyright (c) 2015 procuro. All rights reserved.
//

#import "TMSOutbound.h"

@implementation TMSOutbound

+(TMSOutbound*) global {
    static dispatch_once_t pred = 0;
    __strong static TMSOutbound* _global = nil;
    dispatch_once(&pred, ^{
        _global = [[TMSOutbound alloc] init];
    });
    return _global;
}

@synthesize lastSdrReport = _lastSdrReport;
@synthesize sdrReport = _sdrReport;
@synthesize delivery = _delivery;
@synthesize currentLocation = _currentLocation;
@synthesize hasDelivery = _hasDelivery;
@synthesize navigationRunning = _navigationRunning;

-(id) init {
    if (self = [super init]) {
        _lastSdrReport = nil;
        _sdrReport = nil;
        _delivery = nil;
        _hasDelivery = false;
        _navigationRunning = false;
    }
    return self;
}

- (SdrReport*) lastSdrReport {
    @synchronized (self) {
        return _lastSdrReport;
    }
}

- (SdrReport*) sdrReport {
    @synchronized (self) {
        return _sdrReport;
    }
}

- (SdrDelivery*) delivery {
    @synchronized (self) {
        return _delivery;
    }
}

- (CLLocation*) currentLocation {
    @synchronized (self) {
        return _currentLocation;
    }
}

- (BOOL) hasDelivery {
    @synchronized (self) {
        return _hasDelivery;
    }
}

- (BOOL) navigationRunning {
    @synchronized (self) {
        return _navigationRunning;
    }
}

-(void) setReport:(SdrReport*) sdrReport {
    @synchronized (self) {
        _lastSdrReport = _sdrReport;
        _sdrReport = sdrReport;
        _delivery = sdrReport ? sdrReport.Hierarchy : nil;
        _hasDelivery = _delivery ? true : false;
        _navigationRunning = false;
    }
}

-(void) onMainThread:(void (^)())block {
    if ([NSThread currentThread].isMainThread) {
        block();
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            block();
        });
    }
}

-(void) setSdrReport:(SdrReport*) sdrReport {
    [self setReport:sdrReport];
    [self onMainThread:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"deliverychange"
                                                            object:self
                                                          userInfo:@{ @"changed": [NSNumber numberWithBool:true] }];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"deliveryupdate"
                                                            object:self
                                                          userInfo:@{ @"changed": [NSNumber numberWithBool:true] }];
    }];
}

-(void) updateSdrReport:(SdrReport*) sdrReport {
    [self setReport:sdrReport];
    [self onMainThread:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"deliveryupdate"
                                                            object:self
                                                          userInfo:@{ @"changed": [NSNumber numberWithBool:false] }];
    }];
}

-(void) navigationStarted {
    @synchronized (self) {
        _navigationRunning = true;
    }
}

-(void) updateLocation:(CLLocation*) location {
    @synchronized (self) {
        _currentLocation = location;
    }
    [self onMainThread:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"updatelocation"
                                                            object:self
                                                          userInfo:@{ @"location": location }];
    }];
}

- (id<NSObject>) addObserverForName:(NSString *)name
                              queue:(NSOperationQueue *)queue
                         usingBlock:(void (^)(NSNotification *note))block {
    
    return [[NSNotificationCenter defaultCenter] addObserverForName:name
                                                             object:self
                                                              queue:queue
                                                         usingBlock:block];
}

- (id<NSObject>) addObserverForName:(NSString *)name
                         usingBlock:(void (^)(NSNotification *note))block {
    
    return [[NSNotificationCenter defaultCenter] addObserverForName:name
                                                             object:self
                                                              queue:nil //[NSOperationQueue mainQueue] setting as mainQueue does not appear to actually do antying
                                                         usingBlock:block];
}

@end
