//
//  TMSNavCachedHttp.m
//  PimmMaps
//
//  Created by Sonia Sharma on 19/09/2014.
//  Copyright (c) 2014 procuro. All rights reserved.
//

#import "TMSNavCachedHttp.h"
#import "iPimmNav/iPimmNavLog.h"


@interface CacheEntry : NSObject

@property (nonatomic,copy) NSRegularExpression* pattern;
@property (nonatomic,copy) NSString* value;


-(id) initWithRegexString:(NSString*) regexStr
                    Value:(NSString* )value;

-(id) initWithRegex:(NSRegularExpression*) regex
                    Value:(NSString* )value;


@end

@implementation CacheEntry

-(id) initWithRegexString:(NSString*) regexStr
                    Value:(NSString* )value
{
  //  PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    if(self = [super init])
    {
        NSError* error = nil;
        NSRegularExpression* regex = [[NSRegularExpression alloc] initWithPattern:regexStr options:0 error:&error];
        
        _value = value;
        if(error != nil)
        {
            NSLog(@"Error: %@ while compiling RegexString: %@", error.debugDescription, regexStr);
            return nil;
        }
        else
            _pattern = regex;
        
        return self;
    }
    else
        return nil;
}

-(id) initWithRegex:(NSRegularExpression*) regex
              Value:(NSString* )value
{
  //  PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    if(self = [super init])
    {
        _value = value;
       
        _pattern = regex;
        
        return self;
    }
    else
        return nil;
}

@end

@implementation TMSNavCachedHttp


- (id) init
{
   // PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    if(self = [super init])
    {
        _cache = [[NSMutableArray alloc] init];
        [self loadHereResults];
        return self;
    }
    else
        return nil;
}
-(void) loadHereResults
{
 // PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    
    NSError *jsonError = nil;
  
    NSString* filePath = [[NSBundle mainBundle] pathForResource:@"route_100_here_results" ofType:@"json"];
    
    NSData *jsonData = [[NSData alloc] initWithContentsOfFile:filePath];
    
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
    
    if(jsonError != nil)
    {
        NSLog(@"Error while reading JSON File for response");
        return;
    }
    NSDictionary* resultDic = (NSDictionary*) jsonObject;
    NSArray* responseList = (NSArray*)[resultDic objectForKey:@"responses"];
    
    @try {
        
        
            NSError *jsonError = nil;
        
            NSDictionary* responseDic = [responseList objectAtIndex:0];
            
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseDic options:NSJSONWritingPrettyPrinted  error:&jsonError];
            
            NSString* jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            CacheEntry* entry1 = [[CacheEntry alloc] initWithRegexString:@".+route\\.nlp\\.nokia\\.com/routing/6\\.2/calculateroute\\.json\\?.+&waypoint0=geo!32\\.910970,-117\\.224250&waypoint1=geo!32\\.920400,-117\\.216700" Value:jsonString];
            
            [_cache addObject:entry1];
        
            jsonError = nil;
            responseDic = [responseList objectAtIndex:1];
        
            jsonData = [NSJSONSerialization dataWithJSONObject:responseDic options:NSJSONWritingPrettyPrinted  error:&jsonError];
        
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
            CacheEntry* entry2 = [[CacheEntry alloc] initWithRegexString:@".+route\\.nlp\\.nokia\\.com/routing/6\\.2/calculateroute\\.json\\?.+&waypoint0=geo!32\\.920400,-117\\.216700&waypoint1=geo!32\\.927900,-117\\.236200" Value:jsonString];
        
            [_cache addObject:entry2];
        
        jsonError = nil;
        responseDic = [responseList objectAtIndex:2];
        
        jsonData = [NSJSONSerialization dataWithJSONObject:responseDic options:NSJSONWritingPrettyPrinted  error:&jsonError];
        
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        CacheEntry* entry3 = [[CacheEntry alloc] initWithRegexString:@".+route\\.nlp\\.nokia\\.com/routing/6\\.2/calculateroute\\.json\\?.+&waypoint0=geo!32\\.927900,-117\\.236200&waypoint1=geo!32\\.938900,-117\\.234000" Value:jsonString];
        
        [_cache addObject:entry3];

        
        jsonError = nil;
        responseDic = [responseList objectAtIndex:3];
        
        jsonData = [NSJSONSerialization dataWithJSONObject:responseDic options:NSJSONWritingPrettyPrinted  error:&jsonError];
        
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        CacheEntry* entry4 = [[CacheEntry alloc] initWithRegexString:@".+route\\.nlp\\.nokia\\.com/routing/6\\.2/calculateroute\\.json\\?.+&waypoint0=geo!32\\.938900,-117\\.234000&waypoint1=geo!32\\.979800,-117\\.248900" Value:jsonString];
        
        [_cache addObject:entry4];
        
        jsonError = nil;
        responseDic = [responseList objectAtIndex:4];
        
        jsonData = [NSJSONSerialization dataWithJSONObject:responseDic options:NSJSONWritingPrettyPrinted  error:&jsonError];
        
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        CacheEntry* entry5 = [[CacheEntry alloc] initWithRegexString:@".+route\\.nlp\\.nokia\\.com/routing/6\\.2/calculateroute\\.json\\?.+&waypoint0=geo!32\\.979800,-117\\.248900&waypoint1=geo!32\\.910970,-117\\.224250" Value:jsonString];
        
        [_cache addObject:entry5];



        
        
        
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        
    }
    
    
    
    

    
}

-(BOOL) get:(NSString *)url Callback:(void (^)(NSString*, NSError*))callback
{
   // PimmLogFunctionTrace(@"Enter Function:%s", __func__);
    for(CacheEntry* entry in _cache)
    {
        NSRange range = NSMakeRange(0,url.length);
        if([[entry pattern] numberOfMatchesInString:url options:0 range:range] > 0)
        {
            callback([entry value], nil);
            return true;
        }
    }
    return false;
    
}

@end
