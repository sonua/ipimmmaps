//
//  TMSNavCachedHttp.h
//  PimmMaps
//
//  Created by Sonia Sharma on 19/09/2014.
//  Copyright (c) 2014 procuro. All rights reserved.
//

#import "iPimmNav/TMSHttpHandler.h"

@interface TMSNavCachedHttp : NSObject <TMSHttpHandler>
{
    NSMutableArray* _cache; //List of CacheEntry objects
}

-(id) init;
-(void) loadHereResults;

-(BOOL) get:(NSString *)url Callback:(void (^)(NSString*, NSError*))callback;

@end
