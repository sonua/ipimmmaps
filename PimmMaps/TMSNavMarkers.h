//
//  TMSNavMarkers.h
//  PimmMaps
//
//  Created by Mark Holtze on 12/17/14.
//  Copyright (c) 2014 procuro. All rights reserved.
//

#import <NMAKit/NMAKit.h>
#import <Foundation/Foundation.h>
#import "iPimmDataManagerLib/SdrStop.h"
#import "iPimmDataManagerLib/SdrGisSite.h"
#import "iPimmDataManagerLib/SdrPointOfInterest.h"
#import "iPimmDataManagerLib/SdrDelivery.h"
#import "TMSMapMarker.h"

@class TMSMap;

@interface TMSNavMarkers : NSObject

@property(nonatomic,readonly) NMAMapView* map;
@property(nonatomic,readonly) NMAMapContainer* container;
@property(nonatomic,readonly) int markersZIndex;
@property(nonatomic,readonly) NSArray* stops;
@property(nonatomic,readonly) NSArray* sites;
@property(nonatomic,readonly) NSArray* pois;

@property(nonatomic,strong) void (^stopRendered)(SdrStop*, TMSMapMarker*);
@property(nonatomic,strong) void (^siteRendered)(SdrGisSite*, TMSMapMarker*);
@property(nonatomic,strong) void (^poiRendered)(SdrPointOfInterest*, TMSMapMarker*);

-(id) initWithMap:(NMAMapView*) map;
-(void) destroy;
-(NMAGeoBoundingBox*) getBoundingBox;
-(TMSMapMarker*) getStopById:(NSString*) sid;
-(TMSMapMarker*) getSiteById:(NSString*) sid;
-(TMSMapMarker*) getPOIById:(NSString*) sid;
-(void) setForStops:(NSArray*) stops Sites:(NSArray*) sites POIs:(NSArray*) pois;
-(void) setForDelivery:(SdrDelivery*) delivery;
-(void) removeMarkers;



@end

